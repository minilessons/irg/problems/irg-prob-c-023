/**
 * Gets mouse coordinates
 * @param canvas canvas in which mouse is at
 * @param e event object
 * @returns {{x: (number|*), y: (number|*)}}
 */
function getMouse(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    return {x: pos.x, y: pos.y}
}


/**
 * Main prototypes for CanvasX.
 * Initializes CanvasX prototypes before their usage.
 */
function initPrototypes() {
    /**
     * Redraws whole canvas.
     */
    CanvasX.prototype.invalidate = function () {
        this.valid = false;
        this.draw();
    };

    /**
     * Canvas reset to original clear state.
     * @param state
     */
    CanvasX.prototype.resetCanvas = function (state) {
        this.draggX = 0;
        this.draggY = 0;
        this.dragg = false;

        this.valid = false;
        this.shapes = [];
        this.xOptions.initialSetup = getIdentityMatrix(3, 3); // state;
        this.invalidate();
    };

    /**
     * Function for exporting canvasState.
     *
     * @returns {{transformMatrix: (*|HTMLCollection), rotM: *, scaleM: *, translateM: *, transformF: ({tx, ty, sx, sy, angle}|*)}}
     */
    CanvasX.prototype.exportState = function () {
        let transform = this.getShapeByID(this.constants.strings.letterF)[0].transform;
        let lastAngle = transform.rotation.angle;
        let dx = transform.translation.x;
        let dy = transform.translation.y;
        let scaleX = transform.scale.x;
        let scaleY = transform.scale.y;
        return {
            transformMatrix: this.savedTransform.elements,
            rotM: getCCWRotationMatrix(toRadians(lastAngle)).elements,
            scaleM: getScaleMatrix(scaleX, scaleY).elements,
            translateM: getTranslationMatrix(dx, dy).elements,
            transformF: getTransformF(dx, dy, scaleX, scaleY, lastAngle)
        };
    };

    /**
     * Adds a shape to canvas.
     * @param shape specified shape
     */
    CanvasX.prototype.addShape = function (shape) {
        this.shapes.push(shape);
    };

    /**
     * Clears all canvas drawings on context which canvas represents.
     */
    CanvasX.prototype.clear = function () {
        this.ctx.clearRect(0, 0, this.width, this.height);
    };

    /**
     * Clears shapes array.
     */
    CanvasX.prototype.removeShapes = function () {
        this.shapes = [];
        this.invalidate();
    };

    /**
     * Applies predicate to shape objects.
     *
     * @param shapeID object to apply to
     * @param predicate function to call for each shape with given ID
     */
    CanvasX.prototype.doOnShape = function (shapeID, predicate) {
        this.getShapeByID(shapeID).forEach(predicate);
    };

    /**
     * Checks if canvas contains point provided.
     * @param mx x coordinate of a point
     * @param my y coordinate of a point
     * @returns {boolean} true if canvas contains point
     */
    CanvasX.prototype.contains = function (mx, my) {
        let rect = this.canvas.getBoundingClientRect();
        return ((mx < this.width) && (mx > rect.left) && (my > rect.top) && (my < rect.height));
    };


    /**
     * Draws all specified elements in this canvas. Only if they are not up to date.
     */
    CanvasX.prototype.draw = function () {
        if (!this.valid) {
            let ctx = this.ctx;
            let shapes = this.shapes;
            this.clear();

            // Draw shapes
            let l = shapes.length;
            let objID = this.constants.strings.letterF;
            for (let i = 0; i < l; i++) {
                let sh = shapes[i];
                ctx.save();

                if (sh.ID === "line") {
                    ctx.beginPath();
                    sh.draw(ctx);
                    ctx.restore();
                    continue;
                }
                let initialMatrix;
                if (sh.ID === objID) {
                    initialMatrix = this.xOptions.initialSetup;
                } else {
                    initialMatrix = getIdentityMatrix(3, 3);
                }

                let translateMatrix = getTranslationMatrix(
                    sh.transform.translation.x,
                    sh.transform.translation.y);

                let rotateMatrix = getCCWRotationMatrix(
                    toRadians(sh.transform.rotation.angle)
                );

                // Rotation around point
                let rotX = sh.getRotPoint().x;
                let rotY = sh.getRotPoint().y;
                let originTranslator = getTranslationMatrix(rotX, rotY);
                let invOrigin = getTranslationMatrix(-rotX, -rotY);
                let sMatrix = getScaleMatrix(sh.transform.scale.x, sh.transform.scale.y);

                let rotMatrix =
                    invOrigin.nMultiply(rotateMatrix).nMultiply(originTranslator);
                let scaleMatrix =
                    invOrigin.nMultiply(sMatrix).nMultiply(originTranslator);

                let tm = initialMatrix.nMultiply(scaleMatrix).nMultiply(rotMatrix).nMultiply(translateMatrix);
                this.transform = tm;

                if (sh.ID === objID) {
                    this.savedTransform = tm;
                }

                ctx.setTransform(
                    tm.get(0, 0),
                    tm.get(0, 1),
                    tm.get(1, 0),
                    tm.get(1, 1),
                    tm.get(2, 0),
                    tm.get(2, 1));

                ctx.beginPath();
                sh.draw(ctx);
                ctx.restore();
            }

            this.valid = true;
        }
    };

    /**
     * Gets shapes by identification
     *
     * @param ID identification
     * @returns {Array} list of all shapes which has specified ID
     */
    CanvasX.prototype.getShapeByID = function (ID) {
        let size = this.shapes.length;
        let shapes = this.shapes;
        let retStream = [];
        for (let c = 0; c < size; c++) {
            if (shapes[c].ID !== undefined) {
                if (shapes[c].ID === ID) {
                    retStream.push(shapes[c]);
                }
            }
        }
        return retStream;
    };

    /**
     * Adds canvas interaction.
     *
     * @param myState reference to canvasX
     */
    CanvasX.prototype.addEventListeners = function (myState) {

        function mouseDownCallback(e) {
            mouseDownEvent(e, myState);
        }


        function mouseMoveCallback(e) {
            mouseMoveEvent(e, myState);
        }


        function mouseUpCallback(e) {
            mouseUpEvent(e, myState);
        }


        function keyDownCallback(e) {
            keyDownEvent(e, myState);
        }


        function keyPressCallback(e) {
            keyPressEvent(e, myState);
        }


        // Event listeners
        this.canvas.addEventListener("selectstart", function (e) {
            e.preventDefault();
        }, false);
        this.canvas.addEventListener("contextmenu", function (e) {
            e.preventDefault();
            return false;
        }, false);


        this.canvas.addEventListener("mousedown", mouseDownCallback, true);
        this.canvas.addEventListener("mousemove", mouseMoveCallback, false);
        this.canvas.addEventListener("mouseup", mouseUpCallback, false);
        this.canvas.addEventListener("keydown", keyDownCallback, false);
        this.canvas.addEventListener("keypress", keyPressCallback, false);
    }
}


initPrototypes();