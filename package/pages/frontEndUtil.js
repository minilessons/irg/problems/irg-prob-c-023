/**
 * Gets transformations values.
 * @param tx x translation value
 * @param ty y translation value
 * @param sx x scale value
 * @param sy y scale value
 * @param angle rotation angle value
 * @returns {{tx: *, ty: *, sx: *, sy: *, angle: *}}
 */
function getTransformF(tx, ty, sx, sy, angle) {
    return {
        tx: tx,
        ty: ty,
        sx: sx,
        sy: sy,
        angle: angle
    };
}


/**
 * Adds transform to question state.
 *
 * @param qs question state to add letter F transform to
 */
function addTransformToQS(qs) {
    let t = qs.transformF;
    qs.letFTransform = new Transform(new ScaleTransform(t.sx, t.sy), new TranslateTransform(t.tx, t.ty), new RotationTransform(t.angle, false));
}


/**
 * Transforms hex number (base 16) to decimal (base 10).
 *
 * @param hexNumber hex number to convert to decimal
 * @returns {Number} converted number
 */
function hexToDecimal(hexNumber) {
    return parseInt(hexNumber, 16);
}


/**
 * Function constructor for RGBA color values.
 *
 * @param red amount of red
 * @param green amount of green
 * @param blue amount of blue
 * @param alpha amount of transparency
 * @constructor
 */
function RGBA(red, green, blue, alpha) {
    this.red = red;
    this.green = green;
    this.blue = blue;
    this.alpha = alpha;
    this.cssString = function () {
        return "rgba(" + this.red + "," + this.green + "," + this.blue + "," + this.alpha + ")"
    }
}


/**
 * Converts hex color to rgba.
 *
 * @param hexColor color to convert
 * @param alpha alpha value for new rgba color
 * @returns {RGBA} rgba representation of hex color
 */
function convertHexToRGBA(hexColor, alpha) {
    let red = hexToDecimal(hexColor.slice(1, 3));
    let green = hexToDecimal(hexColor.slice(3, 5));
    let blue = hexToDecimal(hexColor.slice(5, 7));
    return new RGBA(red, green, blue, alpha);
}


/**
 * Sets html canvas properties from constants.
 *
 * @param canvas canvas for which properties are set
 * @param constants constants
 */
function setCanvasAttributes(canvas, constants) {
    canvas.width = constants.numeric.canvasWidth;
    canvas.height = constants.numeric.canvasHeight;
}


/**
 * Algorithm for convex and concave objects. Intersecting ray.
 *
 * @param p point to check
 * @param line line segment
 * @returns {boolean}
 */
function rayIntersectAlgo(p, line) {
    let Py = p.y;
    let Px = p.x;
    let Ax = (line.y1 < line.y2) ? line.x1 : line.x2;
    let Ay = (line.y1 < line.y2) ? line.y1 : line.y2;
    let Bx = (line.y1 > line.y2) ? line.x1 : line.x2;
    let By = (line.y1 > line.y2) ? line.y1 : line.y2;
    let epsilon = 0;
    let m_blue = 0;
    let m_red = 0;


    if (Py === Ay || Py === By) {
        Py = Py + epsilon;
    }

    if ((Py < Ay) || (Py > By)) {
        return false;
    } else if (Px > Math.max(Ax, Bx)) {
        return false;
    } else {
        if (Px < Math.min(Ax, Bx)) {
            return true;
        } else {
            if (Ax !== Bx) {
                m_red = (By - Ay) / (Bx - Ax);
            } else {
                m_red = Math.MAX_SAFE_INTEGER;
            }
            if (Ax !== Px) {
                m_blue = (Py - Ay) / (Px - Ax);
            }
            else {
                m_blue = Math.MAX_SAFE_INTEGER;
            }
            return m_blue >= m_red;
        }
    }
}


/**
 * Helper function for calculating delta value for rotation.
 *
 * @param first value of first angle
 * @param second value of second angle
 * @returns {number} distance from first and second angle
 */
function calculateDelta(first, second) {
    let alphaStart = Math.floor(toDegrees(Math.atan2(first.y, first.x)));
    let alphaCurrent = Math.floor(toDegrees(Math.atan2(second.y, second.x)));
    if (alphaStart < 0) {
        alphaStart += 360;
    }
    if (alphaCurrent < 0) {
        alphaCurrent += 360;
    }

    let result = (alphaStart - alphaCurrent);
    return result < 0 ? 360 + result : result;
}


/**
 * Shows div tag element with specified identification.
 *
 * @param ID id of an element to show
 */
function showDivByID(ID) {
    document.getElementById(ID).style.display = "block";
}


/**
 * Hides div tag element with specified identification.
 *
 * @param ID id of an element to hide
 */
function closeDivByID(ID) {
    document.getElementById(ID).style.display = "none";
}
