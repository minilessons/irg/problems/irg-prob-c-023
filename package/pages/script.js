/*@#include#(constants.js)*/
/*@#common-include#(commonUtil.js)*/

/*@#include#(CanvasEvents.js)*/
/*@#include#(canvasX.js)*/
/*@#include#(grid.js)*/
/*@#include#(canvasProto.js)*/
/*@#include#(canvasShapes.js)*/
/*@#include#(frontEndUtil.js)*/
/*@#common-include#(matrixUtil.js)*/


/**
 * @external questionLocalID
 * @type {string}
 */
let canId = questionLocalID + "_linOp";
let canConst;
let xOptions;
let canvasX;

let gObj = {
    toggleElement: function (id) {
        let elem = document.getElementById(id);
        if (elem.style.display === "none") {
            showDivByID(id);
        } else {
            closeDivByID(id);
        }
    }
};

/**
 * Global Export with name "gObj"
 *
 * @external qExportGlobalObject
 */
qExportGlobalObject(questionLocalID, 'gObj', gObj);

/**
 * Api used to communicate between html and backend data stream
 * @type {{initQuestion: api.initQuestion, takeState: api.takeState, resetState: api.resetState, revertState: api.revertState}}
 */
let api = {
    /**
     * Initializes question with user state and common state
     *
     * @param cst common state object
     * @param userState user data object
     */
    initQuestion: function (cst, userState) {
        /**
         * @external readOnly
         */
        if (readOnly === true) {
            gObj.toggleElement(questionLocalID + "_taskDesc");
            gObj.toggleElement(questionLocalID + "_htuDesc");
        }

        let canvas = document.getElementById(canId);
        canConst = getQuestionConstants(cst.gridSpecs);
        setCanvasAttributes(canvas, canConst);
        xOptions = {
            initialSetup: getIdentityMatrix(3, 3),
            readOnly: readOnly,
            enableText: true,
            gObj: gObj,
            htmlIds: {
                empty: 0
            },
            gridStep: cst.gridSpecs.step
        };

        canvasX =
            new CanvasX(canvas, xOptions, canConst);

        addTransformToQS(userState.qs);
        let step = cst.gridSpecs.step;
        initGrid(canvasX, step);
        initLetter(canvasX, step, userState.qs);

        // Set initial transformation matrix
        canvasX.invalidate();


    },

    /**
     * Export function for taking current and last user entered data
     *
     * @returns {{selectedPixels: (*|Array), pixelValues: (*|Array)}}
     */
    takeState: function () {
        return canvasX.exportState();

    },

    /**
     * Reset function for task redoing.
     *
     * @param cst common state
     * @param emptyState empty state to reset to
     */
    resetState: function (cst, emptyState) {
        canvasX.resetCanvas(new Matrix(emptyState.qs));
        let step = cst.gridSpecs.step;
        addTransformToQS(emptyState.qs);
        initGrid(canvasX, step);
        initLetter(canvasX, step, emptyState.qs);

        canvasX.invalidate();
    },

    /**
     * Reverts the question state to a state which was loaded when page loaded.
     *
     * @param cst common state
     * @param loadedState state at the time of page load
     */
    revertState: function (cst, loadedState) {
        // Reset state.
        canvasX.resetCanvas(new Matrix(loadedState.qs.transformMatrix));

        // Set letterF transformations
        addTransformToQS(loadedState.qs);

        let step = cst.gridSpecs.step;
        initGrid(canvasX, step);
        initLetter(canvasX, step, loadedState.qs);
        canvasX.invalidate();
    }
};


// noinspection JSAnnotator
return api;