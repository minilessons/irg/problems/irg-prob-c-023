/**
 * Object transform: scale
 *
 * @param x horizontal scale value
 * @param y verticals scale value
 * @constructor
 */
function ScaleTransform(x, y) {
    this.x = x || 1;
    this.y = y || 1;

}


/**
 * Object transform: translate
 *
 * @param x horizontal translation value
 * @param y vertical translation value
 * @constructor
 */
function TranslateTransform(x, y) {
    this.x = x || 0;
    this.y = y || 0;

}


/**
 * Object transform: rotation
 *
 * @param angle rotation angle
 * @param CCW direction
 * @constructor
 */
function RotationTransform(angle, CCW) {
    this.CCW = CCW || false;
    this.angle = angle || 0;

    this.setAngle = function (value) {
        if (value >= 360) {
            this.angle = value % 360;
        } else if (value < 0) {
            let times = Math.floor(Math.abs(value) / 360);
            this.angle = (value + (times + 1) * 360);
        } else {
            this.angle = value;
        }
    };
}


/**
 * Transform for any object.
 *
 * @param scale scale transform
 * @param translation translate transform
 * @param rotation rotation transform
 * @constructor
 */
function Transform(scale, translation, rotation) {
    this.scale = scale || {x: 0, y: 0};
    this.rotation = rotation || {angle: 0, CCW: false};
    this.translation = translation || {x: 0, y: 0};

    this.getDefaultTransform = function () {
        return new Transform(new ScaleTransform(1, 1), new TranslateTransform(0, 0), new RotationTransform(0, false));
    }
}


/**
 * Function constructor which initializes a Shape.
 *
 * @param x top left x coordinate of a shape
 * @param y top left y coordinate of a shape
 * @param w width of the shape
 * @param h height of the shape
 * @param fill shape fill
 * @param selected simulates selection of this shape
 * @param ID object identification, for complex objects same ID is given
 * @param isStroke if shape stroke
 * @param rotX x coordinate of shape rotation point
 * @param rotY y coordinate of shape rotation point
 * @constructor
 */
function Shape(x, y, w, h, fill, selected, ID, isStroke, rotX, rotY) {
    this.ID = ID || undefined;
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fill = fill || '#AAAAAA';
    this.selected = selected || false;
    this.selectionColor = "#4123A4";
    this.isStroke = isStroke || false;
    this.rotX = rotX || 0;
    this.rotY = rotY || 0;
    this.transform = (new Transform()).getDefaultTransform();

    this.getRotPoint = function () {
        return {x: this.rotX, y: this.rotY};
    };
}


/**
 * Function constructor for line shape.
 *
 * @param ID id of an object
 * @param x1 start x value of this line
 * @param y1 start y value of this line
 * @param x2 end x value of this line
 * @param y2 end y value of this line
 * @param color line color
 * @param fill line fill
 * @param lineWidth width of the line
 * @param rotX x coordinate of shape rotation point
 * @param rotY y coordinate of shape rotation point
 * @constructor
 */
function Line(ID, x1, y1, x2, y2, color, fill, lineWidth, rotX, rotY) {
    this.ID = ID || undefined;
    this.x1 = x1 || 0;
    this.x2 = x2 || 0;
    this.y1 = y1 || 1;
    this.y2 = y2 || 1;
    this.fillStyle = color || "#000000";
    this.fill = fill || "#000000";
    this.lineWidth = lineWidth || 1;
    this.rotX = rotX || 0;
    this.rotY = rotY || 0;
    this.transform = (new Transform()).getDefaultTransform();

    this.getRotPoint = function () {
        return {x: this.rotX, y: this.rotY};
    };
}


/**
 * Function constructor for text field.
 *
 * @param text text which is shown in text field
 * @param x top left x coordinate for text field
 * @param y top left y coordinate for text field
 * @param w width of the field
 * @param h height of the field
 * @param font font to use for text showcase
 * @param fillStyle fill to use on text field
 * @param textColor color of the text
 * @param textAlign text alignment
 * @param baseline text baseline
 * @param isStroke stroked or filled text: true and false respectively
 * @constructor
 */
function TextField(text, x, y, w, h, font, fillStyle, textColor, textAlign, baseline, isStroke = false) {
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fillStyle = fillStyle || "#000000";
    this.textBaseline = baseline || "middle";
    this.textColor = textColor || "#000000";
    this.font = font || "12px Courier";
    this.fieldText = text || "";
    this.textAlign = textAlign || "center";
    this.isStroke = isStroke || false;
}


/**
 * Function constructor for Arrow shape.
 *
 * @param color arrow fill style
 * @param opts other shape options
 * @constructor
 */
function Arrow(color, opts) {
    this.filled = opts.filled || false;
    this.shadowBlur = opts.shadowBlur;
    this.shadowColor = opts.shadowColor;
    this.fillStyle = color || "#123123";
    this.alphaNonTransparent = opts.alphaNonTransparent || 1;
    this.alphaTransparent = opts.alphaTransparent || 0.5;
    this.transform = (new Transform()).getDefaultTransform();
    this.lines = [];

    // Stroke and fill options
    this.fillShape = opts.fillShape;
    this.strokeShape = opts.strokeShape;

    this.isHovered = false;
    this.selected = false;

    this.setLines = function (lines) {
        this.lines = lines;
    }
}


/**
 * Function constructor for simple Circle shape.
 *
 * @param centerX x value for circle center
 * @param centerY y value for circle center
 * @param radius radius of a circle
 * @param startAngle starting angle
 * @param endAngle ending angle
 * @param rotation CCW or CW rotation
 * @param color stroke color
 * @param fillStyle fill color
 * @param lineWidth width of a circle shape
 * @param opts other options
 * @constructor
 */
function Circle(centerX, centerY, radius, startAngle, endAngle, rotation, color, fillStyle, lineWidth, opts) {
    this.x = centerX;
    this.y = centerY;
    this.radius = radius;
    this.startAngle = startAngle;
    this.endAngle = endAngle;
    this.rotation = rotation;
    this.fillStyle = fillStyle;
    this.lineWidth = lineWidth;
    this.fillStyle = color;

    this.selected = false;
    this.isHovered = false;

    // Stroke and fill options
    this.fillShape = opts.fillShape;
    this.strokeShape = opts.strokeShape;

    this.alphaNonTransparent = opts.alphaNonTransparent || 1;
    this.alphaTransparent = opts.alphaTransparent || 0.5;
    this.transform = (new Transform()).getDefaultTransform();
}


/**
 * Function constructor for letter F.
 *
 * @param ID identification
 * @param rotX x value of rotation origin
 * @param rotY y value of rotation origin
 * @param color fill style
 * @param lineWidth width for the shape lines
 * @param strokeStyle stroke color
 * @param opts other options
 * @constructor
 */
function LetterF(ID, rotX, rotY, color, lineWidth, strokeStyle, opts) {
    this.ID = ID || undefined;
    this.rotX = rotX || 0;
    this.rotY = rotY || 0;
    this.fillStyle = color || "#ddd8da";
    this.lineWidth = lineWidth || 1;
    this.strokeStyle = strokeStyle || "#000000";
    this.lines = [];
    this.scaleArrows = [];
    this.rotCircles = [];

    this.alphaNonTransparent = opts.alphaNonTransparent || 1;
    this.alphaTransparent = opts.alphaTransparent || 0.5;

    this.shadowBlur = opts.shadowBlur;
    this.selectionColor = opts.selectionColor;

    // Stroke and fill options
    this.fillShape = opts.fillShape;
    this.strokeShape = opts.strokeShape;


    this.addRotCircle = function (circle) {
        this.rotCircles.push(circle);
    };

    this.addScaleArrow = function (arrow) {
        this.scaleArrows.push(arrow);
    };

    this.setLines = function (lines) {
        this.lines = lines;
    };
    this.getRotPoint = function () {
        return {x: this.rotX, y: this.rotY};
    };

    this.setRotX = function (x) {
        this.rotX = x;
    };

    this.setRotY = function (y) {
        this.rotY = y;
    };

    this.transform = (new Transform()).getDefaultTransform();
}


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Draw functions for all shapes */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/**
 * Actual drawing of shape Arrow.
 *
 * @param ctx context to draw to
 */
Arrow.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(this.lines[0].x1, this.lines[0].y1);
    this.lines.forEach((line) => {
        ctx.lineTo(line.x2, line.y2);
    });
    if (this.fillShape) {
        ctx.fill();
    }
    if (this.strokeShape) {
        ctx.stroke();
    }
    ctx.restore();
};

/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Arrow.prototype.draw = function (ctx) {
    setupContext(ctx, this);
    if (this.isHovered === true) {
        ctx.save();
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.save();
        setNoSelection(ctx);
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        this.drawShape(ctx);
        ctx.restore();
    }
};


/**
 * Actual drawing of shape Circle.
 *
 * @param ctx context to draw to
 */
Circle.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle, this.rotation);
    if (this.fillShape) {
        ctx.fill();
    }
    if (this.strokeShape) {
        ctx.stroke();
    }
    ctx.restore();
};


/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Circle.prototype.draw = function (ctx) {
    setupContext(ctx, this);
    if (this.selected === true) {
        ctx.save();
        //setDefaultSelection(ctx);
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        this.drawShape(ctx);
        ctx.restore();
    }

};


/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Shape.prototype.draw = function (ctx) {
    ctx.save();

    // Draw selection
    if (this.selected === true) {
        let off = 5;
        ctx.strokeStyle = this.selectionColor;
        ctx.strokeRect(this.x - off, this.y - off, this.w + 2 * off, this.h + 2 * off);
    }

    ctx.fillStyle = this.fill;
    if (this.isStroke === true) {
        ctx.strokeRect(this.x, this.y, this.w, this.h);
    } else {
        ctx.fillRect(this.x, this.y, this.w, this.h);
        ctx.stroke();
    }

    ctx.restore();
};

/**
 * Prototype which is used to draw Line object.
 *
 * @param ctx context to draw to
 */
Line.prototype.draw = function (ctx) {
    ctx.save();
    ctx.strokeStyle = this.fill;
    ctx.color = this.fillStyle;
    ctx.lineWidth = this.lineWidth;
    ctx.moveTo(this.x1, this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.stroke();
    ctx.restore();
};

/**
 * Prototype which is used to draw TextField object.
 *
 * @param ctx context to draw to
 */
TextField.prototype.draw = function (ctx) {
    ctx.save();
    ctx.textAlign = this.textAlign || ctx.textAlign;
    ctx.textBaseline = this.textBaseline || ctx.textBaseline;
    ctx.strokeStyle = this.strokeStyle || ctx.strokeStyle;
    ctx.fillStyle = this.fillStyle || ctx.fillStyle;
    ctx.font = this.font || ctx.font;

    let textX = (this.textAlign === "center") ? this.x + (this.w / 2) : this.x;
    let textY = (this.textAlign === "center") ? this.y + (this.h / 2) : this.y;

    if (this.isStroke === true) {
        ctx.strokeText(this.fieldText, textX, textY);
        ctx.stroke();
    } else {
        ctx.fillText(this.fieldText, textX, textY);
        ctx.stroke();
    }
    ctx.restore();
};

/**
 * Actual drawing of this shape.
 *
 * @param ctx context to draw to
 */
LetterF.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(this.lines[0].x1, this.lines[0].y1);
    this.lines.forEach((line) => {
        ctx.lineTo(line.x2, line.y2);
    });

    if (this.fillShape) {
        ctx.fill();
    }
    if (this.strokeShape) {
        ctx.stroke();
    }
    ctx.restore();
};

/**
 * Prototype which is used to draw LetterF object.
 *
 * @param ctx
 */
LetterF.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    if (this.selected === true) {
        ctx.save();
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        this.drawShape(ctx);
    }

    ctx.restore();


    this.scaleArrows.forEach((arr) => {
        ctx.save();
        arr.draw(ctx);
        ctx.restore();
    });


    this.rotCircles.forEach((circle) => {
        ctx.save();
        circle.draw(ctx);
        ctx.restore();
    });
};


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Checkers for point inside shapes */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/**
 * Checks whether mouse is contained inside arrow.
 *
 * @param refToF reference to object which transformation is used
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {*} true if contains
 */
Arrow.prototype.contains = function (refToF, mx, my) {
    let p = transformPoint(refToF, mx, my);

    // Shooting ray algorithm for convex AND concave bodies.
    let count = 0;
    this.lines.forEach((line) => {
        if (rayIntersectAlgo(p, line)) {
            ++count;
        }
    });
    return isOdd(count);
};


/**
 * Checks whether mouse is contained inside circle.
 *
 * @param bossObj reference to object which transformation is used
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
Circle.prototype.contains = function (bossObj, mx, my) {
    let p = transformPoint(bossObj, mx, my);
    let r = this.radius;
    let dx = p.x - this.x;
    let dy = p.y - this.y;
    return ((dx * dx) + (dy * dy)) < (r * r);
};


/**
 * Checks whether Shape object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Shape.prototype.contains = function (mx, my) {
    return (mx > this.x) && (mx < (this.x + this.w)) &&
        (my > this.y) && (my <= (this.y + this.h));
};

/**
 * Checks whether Line object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Line.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};


/**
 * Checks whether TextField object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
TextField.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};

/**
 * Checks whether mouse is inside this shape.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {*}
 */
LetterF.prototype.contains = function (mx, my) {
    // Set mouse to origin of the object
    let p = transformPoint(this, mx, my);
    // Shooting ray algorithm for convex AND concave bodies.
    let count = 0;
    this.lines.forEach((line) => {
        if (rayIntersectAlgo(p, line)) {
            ++count;
        }
    });
    return isOdd(count);
};


/**
 * Sets default selection.
 *
 * @param ctx context to set selection to
 * @param shadowBlur amount of blur for shadow
 * @param shadowColor color of shadow
 */
function setDefaultSelection(ctx, shadowBlur, shadowColor) {
    ctx.shadowBlur = shadowBlur || 12;
    ctx.shadowColor = shadowColor || "#bf2228"
}


/**
 * Sets up context from given shape object.
 *
 * @param ctx context to setup (defaults to itself if any attribute for undefined shape properties)
 * @param o shape object
 */
function setupContext(ctx, o) {
    // Color and shadow options
    ctx.fillStyle = o.fillStyle || ctx.fillStyle;
    ctx.strokeStyle = o.strokeStyle || ctx.strokeStyle;
    ctx.shadowColor = o.shadowColor || ctx.shadowColor;
    ctx.shadowBlur = o.shadowBlur || ctx.shadowBlur;
    ctx.shadowOffsetX = o.shadowOffsetX || ctx.shadowOffsetX;
    ctx.shadowOffsetY = o.shadowOffsetY || ctx.shadowOffsetY;

    if (o.transform !== undefined) {
        ctx.shadowOffsetX -= o.transform.translation.x;
        ctx.shadowOffsetY -= o.transform.translation.y;
    }

    // Line style
    ctx.lineCap = o.lineCap || ctx.lineCap;
    ctx.lineWidth = o.lineWidth || ctx.lineWidth;
    ctx.setLineDash(o.lineDash || []);

    // Font properties
    ctx.font = o.font || ctx.font;
    ctx.textAlign = o.textAlign || ctx.font;
    ctx.textBaseline = o.textBaseline || ctx.textBaseline;

}


/**
 * Set no selection options.
 *
 * @param ctx context to set selection to
 */
function setNoSelection(ctx) {
    ctx.shadowBlur = 0;
    ctx.shadowColor = "#000000";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
}