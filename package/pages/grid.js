/**
 * Initializes grid, vertical and horizontal lines
 * @param can canvas to draw to
 * @param unitWidth distance from two lines
 */
function initGrid(can, unitWidth) {
    let startX = 0;
    let step = unitWidth || 35;
    let startY = 0;

    let verStart = 0;
    let horStart = 0;
    let verEnd = can.height;
    let horEnd = can.width;
    let c = can.constants;

    let origin = calcOrigin(can, step);

    // Invert axis because top left is y = 0;
    let yAxis = origin.y;
    let xAxis = origin.x;

    // Vertical bars
    let verticalAxis;
    for (let x = startX; x < (horEnd + step); x += step) {
        if (x === xAxis) {
            verticalAxis = new Line("line", x, verStart, x, verEnd, c.colors.axisColor, c.colors.axisColor, c.widths.axisWidth);
        } else {
            let shape = new Line("line", x, verStart, x, verEnd, c.colors.gridColor, c.colors.gridColor);
            can.addShape(shape);
        }
    }

    // Horizontal bars
    let horizontalAxis;
    for (let y = startY; y < (step + verEnd); y += step) {
        if (y === yAxis) {
            horizontalAxis = new Line("line", horStart, y, horEnd, y, c.colors.axisColor, c.colors.axisColor, c.widths.axisWidth);
        } else {
            let shape = new Line("line", horStart, y, horEnd, y, c.colors.gridColor, c.colors.gridColor);
            can.addShape(shape);
        }
    }

    can.addShape(horizontalAxis);
    can.addShape(verticalAxis);
}


/**
 * Helper function for setting transformations for letter F from question state.
 * @param letF instance of letter F
 * @param qs question state object
 */
function setFTransformations(letF, qs) {
    letF.transform = qs.letFTransform;
}


/**
 * Initializes letter F.
 * @param can canvas to add shapes to
 * @param unitWidth width of one grid element
 * @param qs question state (for setting initial transformations)
 */
function initLetter(can, unitWidth, qs) {
    let step = unitWidth;
    let origin = calcOrigin(can, step);
    let c = can.constants;
    let ID = c.strings.letterF;


    let xStart = origin.x;
    let yStart = origin.y;


    let fillTrue = true;
    let strokeTrue = true;

    // Construct letterF
    let letOpts = {
        shadowBlur: 7,
        selectionColor: c.colors.selectionColorF,
        alphaNonTransparent: c.colors.transparency.nonTransparent,
        alphaTransparent: c.colors.transparency.transparent,
        fillShape: fillTrue,
        strokeShape: strokeTrue
    };
    let lineName = "lxx";
    let lineWidth = 1;
    let letterF = new LetterF(ID, xStart, yStart, c.colors.letterF, lineWidth, undefined, letOpts);
    let l1 = new Line(lineName, xStart, yStart, xStart + step, yStart);
    let l2 = new Line(lineName, l1.x2, l1.y2, l1.x2, l1.y1 - 2 * step);
    let l3 = new Line(lineName, l2.x2, l2.y2, l2.x2 + step, l2.y2);
    let l4 = new Line(lineName, l3.x2, l3.y2, l3.x2, l3.y2 - step);
    let l5 = new Line(lineName, l4.x2, l4.y2, l4.x2 - step, l4.y2);
    let l6 = new Line(lineName, l5.x2, l5.y2, l5.x2, l5.y2 - step);
    let l7 = new Line(lineName, l6.x2, l6.y2, l6.x2 + 2 * step, l6.y2);
    let l8 = new Line(lineName, l7.x2, l7.y2, l7.x2, l7.y2 - step);
    let l9 = new Line(lineName, l8.x2, l8.y2, l8.x2 - 3 * step, l8.y2);
    let l10 = new Line(lineName, l9.x2, l9.y2, l9.x2, l9.y2 + 5 * step);

    let linesArr = [l1, l2, l3, l4, l5, l6, l7, l8, l9, l10];
    letterF.setLines(linesArr);

    // Set transformations to letterF;
    if (qs !== undefined) {
        setFTransformations(letterF, qs);
    }

    // Crystal letter
    let cLetterF = new LetterF(ID + "notMovable", xStart, yStart, c.colors.crystal, lineWidth, undefined, letOpts);
    cLetterF.setLines(linesArr);
    can.addShape(cLetterF);
    can.addShape(letterF);

    // Add scale arrows
    let level = 5;
    let smallerWidth = step;
    let arrowHeight = step / 2;
    let distanceFromTop = 10;
    let leftBottom = {
        x: xStart + 2 * step,
        y: yStart - (level * step)
    };

    let middle = {
        x: leftBottom.x + smallerWidth / 2,
        y: leftBottom.y - arrowHeight
    };

    let rightBottom = {
        x: leftBottom.x + smallerWidth,
        y: leftBottom.y
    };

    let dx = leftBottom.x - middle.x;
    let dy = leftBottom.y - middle.y;
    let slope = dy / dx;

    let leftBottomBigger = {
        x: leftBottom.x,
        y: leftBottom.y - distanceFromTop
    };
    let b = leftBottomBigger.y - slope * leftBottomBigger.x;
    let height = slope * middle.x + b;

    let middleBigger = {
        x: middle.x,
        y: height
    };

    let rightBottomBigger = {
        x: rightBottom.x,
        y: rightBottom.y - distanceFromTop
    };

    let arrLId = "aLxx";
    let aL1 = new Line(arrLId, leftBottom.x, leftBottom.y, leftBottomBigger.x, leftBottomBigger.y);
    let aL2 = new Line(arrLId, aL1.x2, aL1.y2, middleBigger.x, middleBigger.y);
    let aL3 = new Line(arrLId, aL2.x2, aL2.y2, rightBottomBigger.x, rightBottomBigger.y);
    let aL4 = new Line(arrLId, aL3.x2, aL3.y2, rightBottom.x, rightBottom.y);
    let aL5 = new Line(arrLId, aL4.x2, aL4.y2, middle.x, middle.y);
    let aL6 = new Line(arrLId, aL5.x2, aL5.y2, aL1.x1, aL1.y1);


    let opts = {
        alphaTransparent: c.colors.transparency.transparent,
        alphaNonTransparent: c.colors.transparency.nonTransparent,
        shadowColor: c.colors.arrowShadow,
        shadowBlur: 30,
        fillShape: fillTrue,
        strokeShape: strokeTrue
    };

    let arrowLines = [aL1, aL2, aL3, aL4, aL5, aL6];
    let arrow = new Arrow(c.colors.arrows, opts);
    arrow.setLines(arrowLines);
    letterF.addScaleArrow(arrow);

    // Construct rotation circle
    let r = Math.PI * 2;
    let startAngle = 0;
    let endAngle = 360;
    let circleColor = c.colors.rotateCircle;
    let fillStyle = c.colors.rotateCircle;
    let yOff = -5;
    let centerX = xStart + 3.5 * step;
    let centerY = yStart - (level * step) + yOff;

    let circleOpts = {
        alphaTransparent: c.colors.transparency.transparent,
        alphaNonTransparent: c.colors.transparency.nonTransparent,
        fillShape: fillTrue,
        strokeShape: strokeTrue
    };

    // AntiClockwise
    let rotation = false;
    let lWidth = 1;
    let RotCircle = new Circle(centerX, centerY, r, startAngle, endAngle, rotation, circleColor, fillStyle, lWidth, circleOpts);

    letterF.addRotCircle(RotCircle);
}

