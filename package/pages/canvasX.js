/**
 * Canvas state represents an object which holds all information about canvas.
 * Does translations, scaling and drawing of object by itself. Can be modified in any way.
 * @param canvas holds HTML canvas object
 * @param xOptions object which holds various properties for this canvas
 * @param constants constants used by canvas
 * @constructor
 */
function CanvasX(canvas, xOptions, constants) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');

    // Canvas properties
    this.width = canvas.width;
    this.height = canvas.height;

    this.xOptions = xOptions;
    this.constants = constants;

    // Canvas mouse tracker
    this.enableMouseTracking = false;
    this.mouseX = 0;
    this.mouseY = 0;
    this.mouseLabel = "mx: " + this.mouseX + ",  my: " + this.mouseY;

    // Properties for tracking canvas state and canvas objects
    this.draggX = 0;
    this.draggY = 0;
    this.dragg = false;
    this.draggScale = false;
    this.rotDragg = false;

    this.valid = false;
    this.shapes = [];
    this.transform = getIdentityMatrix(3, 3);
    this.savedTransform = getIdentityMatrix(3, 3);
    this.initialPositionX = 0;
    this.initialPositionY = 0;
    this.initialScaleX = 1;
    this.initialScaleY = 1;
    this.initialRotation = 0;

    // Reference to CanvasX
    let myState = this;

    // Add Canvas interaction only if canvas is allowed to change.
    if (xOptions.readOnly === false) {
        this.addEventListeners(myState);
    }
}