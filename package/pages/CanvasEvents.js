/**
 * Event for mouse down.
 *
 * @param e event object
 * @param myState reference to state
 */
function mouseDownEvent(e, myState) {
    let canvas = myState.canvas;
    let mouse = getMouse(canvas, e);

    // Revert coordinate system from 0,0 in top left to low left.
    let reversed = myState.canvas.height - mouse.y;
    let objID = myState.constants.strings.letterF;
    let letF = myState.getShapeByID(objID)[0];

    if (letF.contains(mouse.x, mouse.y) === true) {
        myState.dragg = true;
        myState.initialPositionX = letF.transform.translation.x;
        myState.initialPositionY = letF.transform.translation.y;
        myState.draggX = mouse.x;
        myState.draggY = reversed;
    }

    if (true === letF.scaleArrows[0].contains(letF, mouse.x, mouse.y)) {
        myState.draggScale = true;
        myState.draggX = mouse.x;
        myState.draggY = reversed;
        myState.initialScaleX = letF.transform.scale.x;
        myState.initialScaleY = letF.transform.scale.y;
    }

    if (true === letF.rotCircles[0].contains(letF, mouse.x, mouse.y)) {
        myState.rotDragg = true;
        myState.initialRotation = letF.transform.rotation.angle;
        myState.draggX = mouse.x;
        myState.draggY = reversed;
        myState.startingX = mouse.x;
        myState.startingY = reversed;
    }

    myState.invalidate();
}


/**
 * Mouse move event.
 *
 * @param e event object
 * @param myState reference to state
 */
function mouseMoveEvent(e, myState) {
    let canvas = myState.canvas;
    let mouse = getMouse(canvas, e);
    // Set mouse tracker values:
    myState.mouseX = Math.round(mouse.x);
    myState.mouseY = Math.round(mouse.y);
    myState.mouseLabel = "mx: " + myState.mouseX + ",  my: " + myState.mouseY;

    // Deal with dragging object, rotating and scaling by mouse.
    let objID = myState.constants.strings.letterF;
    let dx = mouse.x - myState.draggX;
    let dy = (myState.canvas.height - mouse.y) - myState.draggY;

    if (myState.dragg) {
        let startPosX = myState.initialPositionX;
        let startPosY = myState.initialPositionY;
        let step = myState.xOptions.gridStep;
        let nextPosX = startPosX;
        let nextPosY = startPosY;
        let border = Math.floor(step);

        let moved = false;
        if (dx !== 0) {
            let times = Math.floor(Math.abs(dx) / border);
            if (times === 0) {
                moved = true;
            }
            let translate = border * times;
            nextPosX += (dx > 0) ? translate : -translate;
        }

        if (dy !== 0) {
            let times = Math.floor(Math.abs(dy) / border);
            let translate = border * times;
            if (times === 0) {
                moved = true;
            }
            nextPosY += (dy < 0) ? translate : -translate;
        }

        if (nextPosX !== startPosX || nextPosY !== startPosY || moved === true) {
            myState.doOnShape(objID, function (shape) {
                shape.transform.translation.x = nextPosX;
                shape.transform.translation.y = nextPosY;
            });

            myState.invalidate();
        }

    } else if (myState.draggScale === true) {
        let step = myState.xOptions.gridStep;
        let border = Math.floor(step);
        let startScaleX = myState.initialScaleX;
        let startScaleY = myState.initialScaleY;
        let nextScaleX = startScaleX;
        let nextScaleY = startScaleY;
        let letF = myState.getShapeByID(objID)[0];
        let translateX = letF.transform.translation.x;
        let translateY = letF.transform.translation.y;

        let origin = calcOrigin(myState.canvas, myState.xOptions.gridStep);
        let moved = false;

        // swap scale axis if rotation is
        let xFromOrigin = myState.draggX - origin.x - translateX;
        let yFromOrigin = myState.draggY - origin.y + translateY;
        let rot = letF.transform.rotation.angle;
        if (rot === 90 || rot === -90 || rot === 270 || rot === -180) {
            let tmp = -dx;
            dx = -dy;
            dy = tmp;
            if ((xFromOrigin < 0 && yFromOrigin < 0) || (xFromOrigin > 0 && yFromOrigin > 0)) {
                dx = -dx;
                dy = -dy;
            }
        }

        if (dx !== 0) {
            let scaleFactor = (dx < 0) ? -1 / 3 : 1 / 3;

            if ((xFromOrigin) < 0) {
                scaleFactor *= -1;
            }

            scaleFactor *= (startScaleX < 0) ? -1 : 1;
            let times = Math.floor(Math.abs(dx) / border);
            nextScaleX += (scaleFactor * times);
            if (times === 0) {
                moved = true;
            }
            if (nextScaleX === 0) {
                nextScaleX += (scaleFactor);
            }
            if (Math.abs(nextScaleX) < 1E-6) {
                nextScaleX = (dx < 0) ? -1 / 5 : 1 / 5;
            }
        }

        if (dy !== 0) {
            let scaleFactor = (dy < 0) ? -1 / 5 : 1 / 5;
            if (yFromOrigin < 0) {
                scaleFactor *= -1;
            }
            scaleFactor *= (startScaleY < 0) ? -1 : 1;
            let times = Math.floor(Math.abs(dy) / border);
            nextScaleY += (scaleFactor * times);
            if (times === 0) {
                moved = true;
            }

            if (nextScaleY === 0) {
                nextScaleY += (scaleFactor);
            }
            if (Math.abs(nextScaleY) < 1E-6) {
                nextScaleY = (dy < 0) ? -1 / 5 : 1 / 5;
            }


        }


        if (nextScaleX !== startScaleX || nextScaleY !== startScaleY || moved === true) {
            myState.doOnShape(objID, function (shape) {
                shape.transform.scale.x = (nextScaleX);
                shape.transform.scale.y = (nextScaleY);
            });
            myState.invalidate();
        }
    } else if (myState.rotDragg === true) {
        let letF = myState.getShapeByID(objID)[0];
        let oldRot = letF.transform.rotation.angle;
        let nextRot = oldRot;
        let translateX = letF.transform.translation.x;
        let translateY = letF.transform.translation.y;
        let origin = calcOrigin(myState.canvas, myState.xOptions.gridStep);

        let currentMouse = {
            x: mouse.x - origin.x - translateX,
            y: (myState.canvas.height - mouse.y) - origin.y + translateY
        };

        let initialMouse = {
            x: myState.draggX - origin.x - translateX,
            y: myState.draggY - origin.y + translateY
        };

        let delta = calculateDelta(initialMouse, currentMouse);
        let sign = delta < 0 ? -1 : 1;

        let rotStep = 45;
        let movement = 40;
        let times = Math.floor(delta / movement);
        if (times !== 0) {
            nextRot = times * rotStep;
            nextRot *= sign;
            nextRot += myState.initialRotation;
        } else {
            nextRot = myState.initialRotation;
        }

        if (nextRot !== oldRot) {
            myState.doOnShape(objID, function (shape) {
                shape.transform.rotation.setAngle(nextRot);
            });
            myState.invalidate();
        }

    }

    let checkSelection = (myState, mouse) => {
        let objID = myState.constants.strings.letterF;
        let letF = myState.getShapeByID(objID)[0];
        let arrows = letF.scaleArrows[0];
        let circle = letF.rotCircles[0];


        let changed = false;
        let oldSelections = [letF.selected, circle.selected, arrows.selected];

        if (myState.draggScale === true) {
            arrows.selected = true;
        } else {
            arrows.selected = arrows.contains(letF, mouse.x, mouse.y);
            arrows.isHovered = arrows.selected;
        }

        if (myState.rotDragg === true) {
            circle.selected = true;
        } else {
            circle.selected = circle.contains(letF, mouse.x, mouse.y);
            circle.isHovered = circle.selected;
        }

        if (myState.dragg === true) {
            letF.selected = true;
        } else {
            letF.selected = letF.contains(mouse.x, mouse.y);
        }

        let newSelections = [letF.selected, circle.selected, arrows.selected];

        let size = oldSelections.length;
        for (let c = 0; c < size; c++) {
            if (oldSelections[c] !== newSelections[c]) {
                changed = true;
                break;
            }
        }

        if (changed === true) {
            myState.invalidate();
        }

    };

    if (!myState.dragg && !myState.draggScale && !myState.rotDragg) {
        checkSelection(myState, mouse);
    }
}


/**
 * Mouse up event.
 *
 * @param e event object
 * @param myState reference to state
 */
function mouseUpEvent(e, myState) {
    let objID = myState.constants.strings.letterF;
    if (myState.dragg || myState.draggScale || myState.rotDragg) {
        myState.dragg = false;
        myState.draggScale = false;
        myState.rotDragg = false;
    }

    // Remove selection
    myState.getShapeByID(objID).forEach((shape) => {
        shape.selected = false;
    });
    myState.invalidate();
}


/**
 * Key down event.
 *
 * @param e event object
 * @param myState reference to state
 */
function keyDownEvent(e, myState) {
    let x = e.which || event.keyCode;
    let objID = myState.constants.strings.letterF;
    let letF = myState.getShapeByID(objID)[0];
    let code = myState.constants.keyCodes;
    let step = myState.xOptions.gridStep;

    let successful = false;
    switch (x) {
        case code.right:
            letF.transform.translation.x += step;
            successful = true;
            break;
        case code.left:
            letF.transform.translation.x -= step;
            successful = true;
            break;
        case code.up:
            letF.transform.translation.y -= step;
            successful = true;
            break;
        case code.down:
            letF.transform.translation.y += step;
            successful = true;
            break;
    }

    if (successful) {
        e.preventDefault();
    }

    myState.invalidate();
}


/**
 * Key press event.
 *
 * @param e event object
 * @param myState reference to state
 */
function keyPressEvent(e, myState) {
    let x = e.which || event.keyCode;
    let y = String.fromCharCode(x).toUpperCase();
    let objID = myState.constants.strings.letterF;

    // Only one element object, first one.
    let letF = myState.getShapeByID(objID)[0];
    let rotStep = myState.constants.numeric.objRotation;
    let oldAngle = letF.transform.rotation.angle;
    let scaleX = 1 / 3;
    let scaleY = 1 / 5;
    let successful = true;
    switch (y) {
        case '+':
            letF.transform.rotation.setAngle(oldAngle - rotStep);
            break;
        case '-':
            letF.transform.rotation.setAngle(oldAngle + rotStep);
            break;
        case '1':
            scaleX *= (letF.transform.scale.x < 0) ? -1 : 1;
            letF.transform.scale.x += scaleX;
            if (Math.abs(letF.transform.scale.x) < 1E-6) {
                letF.transform.scale.x += scaleX;
            }
            break;
        case '3':
            scaleY *= (letF.transform.scale.y < 0) ? -1 : 1;
            letF.transform.scale.y += scaleY;
            if (Math.abs(letF.transform.scale.y) < 1E-6) {
                letF.transform.scale.y += scaleY;
            }
            break;
        case '2':
            scaleX *= (letF.transform.scale.x < 0) ? -1 : 1;
            letF.transform.scale.x -= scaleX;
            if (Math.abs(letF.transform.scale.x) < 1E-6) {
                letF.transform.scale.x -= scaleX;
            }
            break;
        case '4':
            scaleY *= (letF.transform.scale.y < 0) ? -1 : 1;
            letF.transform.scale.y -= scaleY;
            if (Math.abs(letF.transform.scale.y) < 1E-6) {
                letF.transform.scale.y -= scaleY;
            }
            break;

        default:
            successful = false;
    }


    if (successful) {
        e.preventDefault();
    }
    myState.invalidate();
}