/**
 * Returns a random number between min (inclusive) and max (inclusive)
 */
function randNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}


/**
 * Helper function to generate scale values.
 *
 * @param min minimum value to generate random number of
 * @param max maximum value to generate random number of
 * @returns {[number, number]} two random values in array
 */
function generateScales(min, max) {
    return [randNum(min, max), randNum(min, max)];
}


/**
 * Helper function to generate translation values.
 *
 * @param min minimum value to generate random number of
 * @param max maximum value to generate random number of
 * @returns {[number, number]} two random values in array
 */
function generateTranslations(min, max) {
    return [randNum(min, max), randNum(min, max)];
}


/**
 * Helper function to generate rotation angle.
 *
 * @param min minimum value to generate random number of
 * @param max maximum value to generate random number of
 * @returns {number} rotation angle
 */
function generateRotationAngle(min, max) {
    return randNum(min, max);
}


/**
 * Gets correct answer of given transformation values.
 *
 * @param scales scale values
 * @param translates translation values
 * @param angle rotation angle
 * @param rotPoint origin point
 * @returns {{transformMatrix: Matrix, rotM: Matrix, scaleM: Matrix, translationM: Matrix}}
 */
function getCorrectAnswer(scales, translates, angle, rotPoint) {
    let initialMatrix = getIdentityMatrix(3, 3);
    let translateMatrix = getTranslationMatrix(
        translates[0],
        -translates[1]);

    if (angle < 0) {
        angle += 360;
    }
    let rotateMatrix = getCCWRotationMatrix(
        toRadians(-angle)
    );

    // Rotation around point
    let rotX = rotPoint.x;
    let rotY = rotPoint.y;
    let originTranslator = getTranslationMatrix(rotX, rotY);
    let invOrigin = getTranslationMatrix(-rotX, -rotY);
    let sMatrix = getScaleMatrix(scales[0], scales[1]);

    let rotMatrix = invOrigin.nMultiply(rotateMatrix).nMultiply(originTranslator);
    let scaleMatrix = invOrigin.nMultiply(sMatrix).nMultiply(originTranslator);

    let translated = initialMatrix.nMultiply(translateMatrix);
    return {
        transformMatrix: scaleMatrix.nMultiply(rotMatrix).nMultiply(translated),
        rotM: rotateMatrix,
        scaleM: sMatrix,
        translationM: translateMatrix
    };
}


/**
 * Checks whether two matrices are equal.
 * @param first first matrix
 * @param second second matrix
 * @returns {number} one if equal otherwise 0
 */
function pointsForEquality(first, second) {
    if (first.equals(second, 1E-3)) {
        return 1;
    } else {
        return 0;
    }
}


/**
 * Calculates grade of given matrices.
 *
 * @param scale scale values
 * @param translation translation values
 * @param rot rotation angle
 * @param correct correct values for transformations
 * @returns {number}
 */
function checkMatrices(scale, rot, translation, correct) {
    let scalePoints = pointsForEquality(new Matrix(scale), new Matrix(correct.scaleM.elements));
    let rotPoints = pointsForEquality(new Matrix(rot), new Matrix(correct.rotM.elements));
    let translationPoints = pointsForEquality(new Matrix(translation),
        new Matrix(correct.translationM.elements));

    let gradeDivider = 1 / 3;
    return gradeDivider * (scalePoints + rotPoints + translationPoints);
}


/**
 * Gets empty state object.
 *
 * @returns {{tx: number, ty: number, sx: number, sy: number, angle: number}}
 */
function getEmptyState() {
    return {
        tx: 0,
        ty: 0,
        sx: 1,
        sy: 1,
        angle: 0
    };
}