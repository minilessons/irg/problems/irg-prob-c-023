/*@#include#(serverUtil.js)*/
/*@#common-include#(commonUtil.js)*/
/*@#common-include#(matrixUtil.js)*/


/* Main Module
 * Contains backend functions which server side calls in order to setup and evaluate task; it
 * also deals with data stream between frontend and backend.
 */

/**
 * Initializer functions for question. Uses configuration file for setting some variables.
 *
 * @param questionConfig configuration JSON object provided in "config-default.json" file.
 */
function questionInitialize(questionConfig) {
    // init task configuration (see: config.json file for details)
    let step = questionConfig.gridStep;
    let rotStep = questionConfig.rotStep;
    let scaleStep = questionConfig.scaleStep;
    let roundingPrecision = questionConfig.roundingPrecision;
    let height = questionConfig.canHeight;
    let width = questionConfig.canWidth;

    let minRot = 0;
    let minTranslate = -5;
    let maxTranslate = 5;
    let maxRot = 6;
    let maxScale = 1;
    let minScale = 0;
    let scaleArr = generateScales(minScale, maxScale);
    let translateArr = generateTranslations(minTranslate, maxTranslate);

    let signForRotation = (randNum(0, 10) < 5) ? -1 : 1;
    let rotAngle = signForRotation * (rotStep + rotStep * generateRotationAngle(minRot, maxRot));
    let signForScales = randNum(0, 10);


    // let solScale = [1 + scaleArr[0] * scaleStep, 1 + scaleArr[1] * scaleStep];
    let solScale = [1 + scaleArr[0], 1 + scaleArr[1]];
    if (signForScales > 5) {
        solScale[0] *= -1;
        solScale[1] *= -1;
    } else {
        solScale[0] += 1;
        solScale[1] += 1;
    }
    let solTranslate = [step * translateArr[0], step * translateArr[1]];

    let can = {
        width: width,
        height: height
    };

    let rotPoint = calcOrigin(can, step);
    let correctTransform = getCorrectAnswer(solScale, solTranslate, rotAngle, rotPoint);
    let correctFTransform = {
        tx: solTranslate[0],
        ty: -solTranslate[1],
        sx: solScale[0],
        sy: solScale[1],
        angle: -rotAngle
    };

    let emptyState = getEmptyState();

    // Saving information for question and other data which will be sent to front-end

    /**
     * @external userData
     *
     */
    userData.gridSpecs = {
        step: step,
        rotStep: rotStep,
        scaleStep: scaleStep,
        canvasWidth: width,
        canvasHeight: height
    };

    userData.evalOpts = {
        precision: roundingPrecision,
        wrongOrderPercentage: questionConfig.wrongOrderPercentage
    };

    userData.question = {
        correct: correctTransform,
        questionState: {
            transformMatrix: getIdentityMatrix(3, 3).elements,
            rotM: getCCWRotationMatrix(0),
            translateM: getTranslationMatrix(0, 0),
            scaleM: getScaleMatrix(1, 1),
            transformF: emptyState
        }
    };

    userData.correctQuestionState = {
        transformMatrix: getIdentityMatrix(3, 3),
        transformF: correctFTransform
    };

    userData.taskTransformations = {
        scale: [solScale[0].toFixed(1), solScale[1].toFixed(1)],
        translation: translateArr,
        rotation: rotAngle
    };
}


/**
 * Returns computed properties which can be used in frontend as echo directives
 *
 * @returns {{x0, y0: *, x1, y1: *}} coordinates of question start point and end point
 */
function getComputedProperties() {
    return {
        scaleX: userData.taskTransformations.scale[0],
        scaleY: userData.taskTransformations.scale[1],
        translateX: userData.taskTransformations.translation[0],
        translateY: userData.taskTransformations.translation[1],
        angle: userData.taskTransformations.rotation,

    };
}


/**
 * Question evaluator for this question.
 *
 * @returns {{correctness: number, solved: boolean}}
 */
function questionEvaluate() {

    /**
     * Sets correct solution if user did not answer the question 100%.
     */
    function setCorrectSolution() {
        userData.correctQuestionState.transformMatrix = userData.question.correct.transformMatrix.elements;
    }


    let userMatrix = new Matrix(userData.question.questionState.transformMatrix);
    let precision = 1E-3;
    let res = {correctness: 0.0, solved: false};
    let isSolved = !(userMatrix.equals(getIdentityMatrix(3, 3), precision));
    if (isSolved === false) {
        setCorrectSolution();
        return res;
    }

    res.solved = true;


    // Compare matrices
    let userRotation = userData.question.questionState.rotM;
    let userTranslation = userData.question.questionState.translateM;
    let userScale = userData.question.questionState.scaleM;
    let finalGrade = checkMatrices(userScale, userRotation,
        userTranslation, userData.question.correct);
    finalGrade = Number(finalGrade.toFixed(userData.evalOpts.precision));

    if (Math.abs(finalGrade - 1.0) < precision) {
        res.correctness = 1;
        userData.correctQuestionState.transformMatrix = userMatrix.elements;
    } else if (Math.abs(finalGrade) < precision) {
        res.correctness = 0;
        setCorrectSolution();
    } else {
        res.correctness = finalGrade;
        setCorrectSolution();
    }

    return res;
}


/**
 * Exports user state and question information which frontend basic question setup needs
 *
 * @returns Object which have common specifications for frontend
 */
function exportCommonState() {
    return {
        gridSpecs: userData.gridSpecs,
        qs: userData.question.questionState
    };
}


/**
 * Exports question state depending on type asked
 *
 * @param stateType can be user or correct solution
 * @returns {*}
 */
function exportUserState(stateType) {
    if (stateType === "USER") {
        return {qs: userData.question.questionState};
    }
    if (stateType === "CORRECT") {
        return {qs: userData.correctQuestionState};
    }

    return {};
}


/**
 * Exports empty state of user data question state
 *
 * @returns Object state which is a state which has identity matrix defined
 */
function exportEmptyState() {
    return {
        qs: {
            transformMatrix: getIdentityMatrix(3, 3).elements,
            transformF: getEmptyState()
        }
    };
}


/**
 * Imports question state data which is changed by the user in front end and saves it for Database.
 *
 * @param data question state data; question state should be of the same form as exported one
 */
function importQuestionState(data) {
    userData.question.questionState = data;
}