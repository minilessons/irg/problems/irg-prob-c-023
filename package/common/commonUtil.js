/**
 * Checks whether number is odd or even.
 */
function isOdd(num) {
    return (num % 2) !== 0;
}


/**
 * Converts radians to degrees.
 *
 * @param radian number of radians to convert
 * @returns {number} degree value for specified radians
 */
function toDegrees(radian) {
    return (radian * 180) / Math.PI;
}


/**
 * Converts degrees to radians.
 * @param degree number of degrees to convert
 * @returns {number} radian value for specified degrees
 */
function toRadians(degree) {
    return (Math.PI / 180) * degree;
}


/**
 * Inits every element of 2D array with given value.
 * @param rows number of rows that will be initialized
 * @param cols number of columns that will be initialized
 * @param value specifies value used to init all elements of 2D array
 * @returns {Array} 2D array of elements initialized with provided value
 */
function init2DArray(rows, cols, value) {
    let arr = [];
    for (let c = 0; c < rows; c++) {
        arr[c] = [];
        for (let k = 0; k < cols; k++) {
            arr[c][k] = value;
        }
    }
    return arr;
}


/**
 * Helper function for calculating grid origin.
 * @param can canvas to which origin is relative to
 * @param step gap between grid lines
 * @returns {{x: number, y: number}} point x, y located at 0, 0 in local grid system.
 */
function calcOrigin(can, step) {
    let xCoord = Math.floor(step * Math.floor((Math.floor((can.width / step))) / 2));
    let yCoord = Math.floor(step * Math.floor((Math.floor((can.height / step))) / 2));
    return {x: xCoord, y: yCoord}
}
