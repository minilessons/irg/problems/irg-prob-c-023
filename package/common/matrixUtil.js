/**
 * Matrix representation.
 *
 * @param components matrix components
 * @constructor
 */
function Matrix(components) {
    /**
     * Matrix elements.
     */
    this.elements = components;

    /**
     * Gets number of rows.
     */
    this.getRowsCount = function () {
        return this.elements.length;
    };

    /**
     * Gets number of columns.
     */
    this.getColsCount = function () {
        return this.elements[0].length;
    };

    /**
     * Sets element at position row, column with a specified value.
     * @param row row position
     * @param column column position
     * @param value value to set element to
     */
    this.set = function (row, column, value) {
        this.elements[row][column] = value;
    };

    /**
     * Gets value in a matrix at specified position.
     * @param row row position
     * @param column column position
     * @returns {*} matrix element at given position
     */
    this.get = function (row, column) {
        return this.elements[row][column];
    };

    /**
     * Checks whether two matrices are equal up to given precision.
     *
     * @param other matrix to check equality to
     * @param p precision value (defaults to 1E-3)
     * @returns {boolean} true if equal
     */
    this.equals = function (other, p) {
        let precision = p || 1E-3;

        let rows = this.getRowsCount();
        let cols = this.getColsCount();
        if (rows !== other.getRowsCount() && cols !== other.getColsCount()) {
            // throw new MatrixException("Can't do predicate on dimension mismatch!");
            throw "Can't do predicate on dimension mismatch!";
        }
        for (let c = 0; c < rows; c++) {
            for (let k = 0; k < cols; k++) {
                let result = Math.abs(this.get(c, k) - other.get(c, k));
                if (result > precision) {
                    return false;
                }
            }
        }
        return true;
    };

    /**
     * Calculates determinant
     * @returns {*} determinant value
     */
    this.determinant = function () {
        if (this.getColsCount() !== this.getRowsCount()) {
            // throw new MatrixException("Matrix is singular.");
            throw "Matrix is singular.";
        }

        let size = this.getRowsCount();
        if (size === 1) {
            return this.get(0, 0);
        }
        else if (size === 2) {
            return ((this.get(0, 0) * this.get(1, 1)) - (this.get(0, 1) * this.get(1, 0)));
        }
        else {
            let sum = 0;
            for (let p = 0; p < size; p++) {
                sum += this.get(0, p) * Math.pow(-1, p) * this.subMatrix(0, p).determinant();
            }
            return sum;
        }
    };

    /**
     * Transposes matrix
     * @param source matrix to transpose
     * @returns {Array}
     */
    this.transposeComponents = function (source) {
        let rows = source.getRowsCount();
        let cols = source.getColsCount();
        let comps = [];
        for (let c = 0; c < cols; c++) {
            let row = [];
            for (let j = 0; j < rows; j++) {
                row.push(source.get(j, c));
            }
            comps.push(row);
        }
        return comps;
    };

    /**
     * Returns transposed matrix (newly created)
     *
     * @returns {Matrix} transposed matrix
     */
    this.nTranspose = function () {
        // Make new matrix with switched rows and columns
        let comps = this.transposeComponents(this);
        let m = new Matrix(comps);
        return m;
    };

    /**
     * Gets a sub matrix of matrix as sub elements
     *
     * @param source source matrix
     * @param row row position
     * @param column column position
     * @returns {Array} components of sub matrix
     */
    this.subElements = function (source, row, column) {
        let comps = [];
        let rows = source.getRowsCount();
        let cols = source.getColsCount();
        for (let i = 0; i < rows; i++) {
            if (i === row) {
                continue;
            }
            let r = [];
            for (let j = 0; j < cols; j++) {
                if (j !== column) {
                    r.push(source.get(i, j));
                }
            }
            comps.push(r);
        }
        return comps;
    };

    /**
     * New matrix constructed from sub matrix
     * @param row row position
     * @param column column position
     * @returns {Matrix} newly created matrix with specified sub matrix
     */
    this.subMatrix = function (row, column) {
        let comps = this.subElements(this, row, column);
        let m = new Matrix(comps);
        return m;
    };

    /**
     * Gets cofactor matrix of a source matrix.
     *
     * @param source matrix to calculate cofactor of
     * @returns {Matrix} cofactor matrix
     */
    this.getCofactor = function (source) {
        let rows = source.getRowsCount();
        let cols = source.getColsCount();
        let m = new Matrix(init2DArray(rows, cols, 0));

        for (let c = 0; c < rows; c++) {
            for (let pp = 0; pp < cols; pp++) {
                let value = Math.pow(-1, (c + pp)) *
                    (source.subMatrix(c, pp).determinant());
                m.set(c, pp, value);
            }
        }
        return m;
    };

    /**
     * Inverts matrix and returns inverse.
     */
    this.nInvert = function () {
        let det = this.determinant();
        if (det === 0) {
            throw "No inverse! Determinant is zero.";
        }
        let cf = this.getCofactor(this);
        let adjugate = cf.nTranspose();

        let m = adjugate.scalarMultiply(1 / det);
        return m;
    };

    /**
     * Multiplies two matrices.
     *
     * @param m2 second matrix
     * @returns {Matrix} multiplied matrix
     */
    this.nMultiply = function (m2) {
        let m1 = this;
        if (m1.getColsCount() !== m2.getRowsCount()) {
            throw "Invalid matrix dimensions.";
        }

        let rows = m1.getRowsCount();
        let cols = m1.getColsCount();
        let otherCols = m2.getColsCount();
        let elements = init2DArray(rows, otherCols, 1);
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < otherCols; j++) {
                let sum = 0;
                for (let k = 0; k < cols; k++) {
                    sum += (m1.get(i, k) * m2.get(k, j));
                }
                elements[i][j] = sum;
            }
        }
        return new Matrix(elements);
    };

    /**
     * Multiplies matrix with a scalar.
     *
     * @param scalar value to multiply matrix
     * @returns {Matrix} product of a matrix and a scalar
     */
    this.scalarMultiply = function (scalar) {
        let rows = this.getRowsCount();
        let cols = this.getColsCount();
        for (let c = 0; c < rows; c++) {
            for (let pp = 0; pp < cols; pp++) {
                this.set(c, pp, scalar * this.get(c, pp));
            }
        }
        return new Matrix(this.elements);
    };
}


/**
 * Gets identity matrix specified by number of columns and rows
 *
 * @param rows number of rows
 * @param columns number of columns
 * @returns {Matrix} identity matrix with given dimensions
 */
function getIdentityMatrix(rows, columns) {
    let m = [];

    for (let c = 0; c < rows; c++) {
        let row = [];
        for (let k = 0; k < columns; k++) {
            if (c === k) {
                row.push(1);
            } else {
                row.push(0);
            }
        }
        m.push(row);
    }
    return new Matrix(m);
}


/**
 * Gets scale matrix (2D).
 *
 * @param x scale value
 * @param y scale value
 * @returns {Matrix} matrix with 3x3 components set to a scale transformation matrix
 */
function getScaleMatrix(x, y) {
    return new Matrix(
        [
            [x, 0, 0],
            [0, y, 0],
            [0, 0, 1]
        ]
    )
}


/**
 * Gets translation matrix (2D).
 * @param x translate value
 * @param y translate value
 * @returns {Matrix} matrix with 3x3 components set to a translation transformation matrix
 */
function getTranslationMatrix(x, y) {
    return new Matrix([
        [1, 0, 0],
        [0, 1, 0],
        [x, y, 1]
    ]);
}


/**
 * Helper function for counter clockwise transformation matrix.
 *
 * @param angle rotation angle
 */
function getCWRotationMatrix(angle) {
    return getCCWRotationMatrix(-angle);
}


/**
 * Gets rotation matrix (2D).
 *
 * @param angle rotation angle
 * @returns {Matrix} matrix with 3x3 components set to a rotation transformation matrix
 */
function getCCWRotationMatrix(angle) {
    return new Matrix([
        [Math.cos(angle), Math.sin(angle), 0],
        [-1 * Math.sin(angle), Math.cos(angle), 0],
        [0, 0, 1]
    ]);
}


/**
 * Returns matrix of specified vector
 *
 * @param vector vector to use for matrix creation
 * @returns {Matrix}
 */
function toHomogenous(vector) {
    return new Matrix([[vector.get(0, 0), vector.get(0, 1), 1]]);
}


/**
 * Row matrix of two components.
 *
 * @param x x component
 * @param y y component
 * @returns {Matrix} matrix with x, y components
 */
function createRowMatrix(x, y) {
    return new Matrix([[x, y]]);
}


/**
 * Helper function for going back to work space coordinates from homogeneous.
 *
 * @param rowMatrix matrix to convert
 * @returns {{x: number, y: number}} work space coordinates
 */
function toCoordinates(rowMatrix) {
    let homogeneous = rowMatrix.get(0, 2);
    return {
        x: rowMatrix.get(0, 0) / homogeneous,
        y: rowMatrix.get(0, 1) / homogeneous
    };
}


/**
 * Helper function for projection matrix.
 *
 * @param rowMatrix matrix to use for calculations
 * @param mvp model view projection matrix
 *
 * @returns {{x: number, y: number}} projection matrix coordinates
 */
function calculateProjection(rowMatrix, mvp) {
    let res = rowMatrix.nMultiply(mvp);
    return toCoordinates(res);

}


/**
 * Helper function for transforming point with given transformations.
 *
 * @param obj object to which transform is applied
 * @param mx mouse x coordinate
 * @param my mouse y coordinate
 * @returns {{x: number, y: number}}
 */
function transformPoint(obj, mx, my) {
    let transform = obj.transform;
    let S1 = getScaleMatrix(transform.scale.x, transform.scale.y);

    let invS1;
    if (S1.determinant() !== 0) {
        invS1 = S1.nInvert();
    } else {
        invS1 = getScaleMatrix(1, 1);
    }
    let T1 = getTranslationMatrix(transform.translation.x, transform.translation.y);
    let invT1;
    if (T1.determinant() !== 0) {
        invT1 = T1.nInvert();
    } else {
        invT1 = getTranslationMatrix(0, 0);
    }

    let T2 = getTranslationMatrix(obj.getRotPoint().x, obj.getRotPoint().y);
    let invT2;
    if (T2.determinant() !== 0) {
        invT2 = T2.nInvert();
    } else {
        invT2 = getTranslationMatrix(0, 0);
    }
    let R = getCWRotationMatrix(toRadians(transform.rotation.angle));

    let finalMatrix =
        invT1.nMultiply(invT2).nMultiply(R).nMultiply(T2)
            .nMultiply(invT2).nMultiply(invS1).nMultiply(T2);
    return calculateProjection(
        toHomogenous(createRowMatrix(mx, my)),
        finalMatrix);
}



